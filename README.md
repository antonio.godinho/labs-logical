# labs-logical

- **Lab-Malware_Analysis:** Representation of a logical network lab for Malware Analysis
- **Lab-Penetration_Testing:** Representation of a logical network lab for Penetration Testing
- **Lab-Crazy:** Representation of a logical network lab for Penetration Testing and Malware Analysis

## Penetration Testing
![](labs/Lab-Penetration_Testing.jpg)

## Malware Analysis
![](labs/Lab-Malware_Analysis.png)

## Crazy (Penetration Testing + Malware Analysis)
![](labs/Lab-Crazy.png)
